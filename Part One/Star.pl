% Problem #3, "Star Tricked", By Keith King 
% Each person claimed that they have seen UFOs on different days but it turned out to be either a balloon,frisbee, water tower
% or clotheline. This puzzle solution determines the day each person sighted a UFO as well as the object that it turned 
% out to be.

% Defining Facts for the puzzle : possible UFO object , person (Mr./Ms.) and day ( Tuesday to Friday).

sighted_day(tuesday).
sighted_day(wednesday).
sighted_day(thursday).
sighted_day(friday).

ufo_object(balloon).
ufo_object(clothesline).
ufo_object(frisbee).
ufo_object(watertower).

person(ms_Barrada).
person(ms_Gort).
person(mr_Klatu).
person(mr_Nikto).

% Defining Rules for the puzzle.

solve :-


% all_different takes a list as input and verifies that it does not contain duplicate values.
    
    % this make sure each person has sighted different object.
    ufo_object(Barrada_Sighted), ufo_object(Gort_Sighted), ufo_object(Klatu_Sighted), ufo_object(Nikto_Sighted),
    all_different([Barrada_Sighted, Gort_Sighted, Klatu_Sighted, Nikto_Sighted]),
    
    
    % this make sure each person is different.
    person(Ms_Barrada), person(Ms_Gort), person(Mr_Klatu), person(Mr_Nikto),
    all_different([Ms_Barrada, Ms_Gort, Mr_Klatu, Mr_Nikto]),

    Triples = [ [Ms_Barrada, Barrada_Sighted, tuesday],
                [Ms_Gort, Gort_Sighted, wednesday],
                [Mr_Klatu, Klatu_Sighted, thursday],
                [Mr_Nikto, Nikto_Sighted, friday] ],

% Rule # 1. Mr. Klatu made his sighting at some point earlier in the week than the one who saw the balloon, 
% but at some point later in the week than the one who spotted the frisbee (who isn't Ms. Gort).


% the function member returns true if part of the given List

	\+(member([mr_Klatu, balloon, _], Triples)),
	\+(member([mr_Klatu, frisbee, _], Triples)),
	\+(member([ms_Gort, frisbee, _], Triples)),
	
% The new tempfunction is used to check the earlier findings in the week to create the rule for Mr. Klatu's clause.

	tempfunction([mr_Klatu, _, _], [_, balloon, _], Triples),
	tempfunction([_, frisbee, _], [mr_Klatu, _, _], Triples),

%Rule # 2. Friday's sighting was made by either Ms. Barrada or the one who saw a clothesline, OR (;) both

	(member([ms_Barrada, _, friday], Triples) ; member([_, clothesline, friday], Triples)),

% Rule # 3. Mr. Nikto did not make his sighting on Tuesday
	\+(member([mr_Nikto, _, tuesday], Triples)),
   
% Rule #  4. Mr. Klatu isn't the one whose object turned out to be a water tower
    \+(member([mr_Klatu, watertower, _], Triples)),

    tell(Ms_Barrada, Barrada_Sighted, tuesday),
    tell(Ms_Gort, Gort_Sighted, wednesday),
    tell(Mr_Klatu, Klatu_Sighted, thursday),
    tell(Mr_Nikto, Nikto_Sighted, friday).

% Succeeds if all elements of the argument list are bound and different.
% Fails if any elements are unbound or equal to some other element.

all_different([H | T]) :- member(H, T), !, fail.
all_different([_ | T]) :- all_different(T).
all_different([_]).


% This temp function is defined below based on requirement of three parameters for rule#1 and customized based 
% on the above given all_different() function.

tempfunction(X, _, [X | _]).
tempfunction(_, Y, [Y | _]) :- !, fail.
tempfunction(X, Y, [_ | T]) :- tempfunction(X, Y, T). 

% prints the output

tell(X, Y, Z) :-
    write('The person '), write(X), write(' misinterpreted a '), write(Y),write(' with the UFO on '), write(Z), 
    write('.'), nl.