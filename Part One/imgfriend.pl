% Problem #2, "Imaginary Friends", By Keith King 
% Each child describes adventure involving imaginary friend at some location. All four children have 
% different Animals and at distinct locations.

% Defining Facts for the puzzle : Child , Animal and Location.



animal(grizzly_bear).
animal(moose).
animal(seal).
animal(zebra).

child(joanne).
child(lou).
child(ralph).
child(winnie).


location(circus).
location(rock_band).
location(spaceship).
location(train).


% Defining Rules for the puzzle.

solve :-
    animal(JoanneImgFnd), animal(LouImgFnd), animal(RalphImgFnd), animal(WinnieImgFnd),
    
    all_different([JoanneImgFnd, LouImgFnd, RalphImgFnd, WinnieImgFnd]),
    
    location(Joannelocation), location(Loulocation),location(Ralphlocation), location(Winnielocation),
    
    all_different([Joannelocation, Loulocation, Ralphlocation, Winnielocation]),

    Triples = [ [joanne, JoanneImgFnd, Joannelocation],
                [lou, LouImgFnd, Loulocation],
                [ralph, RalphImgFnd, Ralphlocation],
                [winnie, WinnieImgFnd, Winnielocation] ],
   
% Rule #  1. The seal(who is the creation of either Joanne or Lou) neither rode to the moon in a spaceship 
% nor took a trip around the world on a magic train.

    \+ member([joanne, _, spaceship], Triples),
    \+ member([joanne, _, train], Triples),

      
    \+ member([lou, _, spaceship], Triples),
    \+ member([lou, _, train], Triples),
  
    
% Rule # 2. Joanne's imaginary friend (who is not the grizzly bear) went to the circus.

    \+ member([joanne, grizzly_bear,_ ], Triples),
    \+ member([joanne, _,spaceship ], Triples),
    \+ member([joanne, _,train ], Triples),
    \+ member([joanne, _,rock_band ], Triples),
   
    
% Rule # 3. Winnie's imaginary friend is a Zebra.
    \+ member([joanne, zebra,_ ], Triples),
    \+ member([lou,zebra, _], Triples),
    \+ member([ralph,zebra, _], Triples),
     
    
% Rule # 4. The grizzly bear did not board the spaceship to the moon.
    \+ member([_, grizzly_bear , moon], Triples),

    

    tell(joanne,JoanneImgFnd, Joannelocation),
    tell(lou,LouImgFnd,Loulocation),
    tell(ralph, RalphImgFnd, Ralphlocation),
    tell(winnie,WinnieImgFnd, Winnielocation).
    
    
% Succeeds if all elements of the argument list are bound and different.
% Fails if any elements are unbound or equal to some other element.

all_different([H | T]) :- member(H, T), !, fail.
all_different([_ | T]) :- all_different(T).
all_different([_]).

% prints the output

tell(X, Y, Z) :-
	
    write('The young author '), write(X), write(' went to the '), write(Z),
    write(' with his imaginary friend '), write(Y), nl.