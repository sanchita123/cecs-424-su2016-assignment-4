/* "Frozen - Escaping Elsa from Prince Hans" : Author - Sanchita Srivastava
   Start the game with the command:   start.  */
   
/* This is a single player fiction based game
 and You are Princess Ana who wants to free Queen Elsa from the prison. */
/*Elsa has been kept in her castle by Prince Hans who wants to exploit Kingdom Arendelle for his business interest.*/
/* Queen Elsa possesses cryptokinetic powers by which she inadvertently made the Kingdom Arendelle almost frozen.*/
 

:- dynamic at/2, i_am_at/1, alive/1, kill/1.  /* Needed by SWI-Prolog. */

/* This defines Princess Ana's current location. */

i_am_at(hall).


/* These facts describe how the rooms are connected. */

path(hall, n, room_entrance).
path(room_entrance, s, hall).

path(room_entrance, e, room).
path(room, w, room_entrance).  

path(room, u, snow_monster).
path(snow_monster, d, room).

%path(snow_monster, d, room):- at(gun, in_hand).
%path(snow_monster, d, room):-  write('with snow monster.'), nl,
 %       fail.



%path(room, w, room_entrance).
%path(room, w, room_entrance):- at(key, in_hand).
%path(room, w, room_entrance) :-  write('inside room.'), nl,
 %       fail.

  




       


/* These facts tell where the various objects in the game
   are located. */

at(elsa, snow_monster).
at(key, room_entrance).
at(gun, room).


/* This fact specifies that the snow_monster is alive. */

alive(snow_monster).


/* These rules describe how to pick up an object. */

take(X) :-
        at(X, in_hand),
        write('You''re already holding it!'),
        nl.

take(X) :-
        i_am_at(Place),
        at(X, Place),
        retract(at(X, Place)),
        assert(at(X, in_hand)),
        write('OK.'),
        nl.

take(_) :-
        write('I don''t see it here.'),
        nl.


/* These rules describe how to put down an object. */

drop(X) :-
        at(X, in_hand),
        i_am_at(Place),
        retract(at(X, in_hand)),
        assert(at(X, Place)),
        write('OK.'),
        nl.

drop(_) :-
        write('You aren''t holding it!'),
        nl.


/* These rules define the six direction letters as calls to go/1. */

n :- go(n).

s :- go(s).

e :- go(e).

w :- go(w).

u :- go(u).

d :- go(d).


/* This rule tells how to move in a given direction. */

go(Direction) :-
        i_am_at(Here),
        path(Here, Direction, There),
        retract(i_am_at(Here)),
        assert(i_am_at(There)),
        look.

go(_) :-
        write('You can''t go that way.').


/* This rule tells how to look about you. */

look :-
        i_am_at(Place),
        describe(Place),
        nl,
        notice_objects_at(Place),
        nl.


/* These rules set up a loop to mention all the objects
   in your vicinity. */

notice_objects_at(Place) :-
        at(X, Place),
        write('There is a '), write(X), write(' here.'), nl,
        fail.

notice_objects_at(_).


/* These rules tell how to kill the snow_monster. */

kill :-
        i_am_at(room),
        write('This isn''t working.  The snow_monster is very strong'), nl.
      

kill :-
        i_am_at(snow_monster),
        at(gun, in_hand),
        retract(alive(snow_monster)),
        write('You hack repeatedly at the snow_monster''s back.'), nl,
        write('I think you have killed it.'),
        nl.

kill :-
        i_am_at(snow_monster),
        write('You have killed the snow monster..Queen Elsa is rescued.'), nl.
 

kill :-
        write('I see nothing harmful here.'), nl.


/* This rule tells how to die. */

die :-
        finish.



finish :-
        nl,
        write('The game is over. Please enter the   halt.   command.'),
        nl.


/* This rule just writes out game instructions. */

instructions :-
        nl,
        write('Enter commands using standard Prolog syntax.'), nl,
        write('Available commands are:'), nl,
        write('start.                   -- to start the game.'), nl,
        write('n.  s.  e.  w.  u.  d.   -- to go in that direction.'), nl,
        write('take(Object).            -- to pick up an object.'), nl,
        write('drop(Object).            -- to put down an object.'), nl,
        write('kill.                    -- to attack an enemy.'), nl,
        write('look.                    -- to look around you again.'), nl,
        write('instructions.            -- to see this message again.'), nl,
        write('halt.                    -- to end the game and quit.'), nl,
        nl.


/* This rule prints out instructions and tells where you are. */

start :-
        instructions,
        look.


/* These rules describe the various rooms.  Depending on
   circumstances, a room may have more than one description. */

describe(hall) :-
        at(elsa, in_hand),
        write('Bravo!!  You have recovered the Queen Elsa'), nl,
        write('and won the game.'), nl,
        finish.

describe(hall) :-
        write('You are in a Hall.  To the north is the entrance'), nl,
        write('of a room..  Your aim is to free the Queen Elsa from Snow Monster'), nl.
    
describe(room) :-
    
        write(' Beware Ana ! There is a giant snow_monster with big mouth to blow you out.'), nl.



describe(room_entrance) :-

        write('You are in front of the room.'), nl,
		write('You have got the key and door is unlocked now.'), nl,
        write('To the east, there is a room where Queen Elsa is being captured.'), nl, !.

describe(room_entrance) :-

	  at(key, in_hand),
        write('You are in front of the room.'), nl,
		write('____You have got the key and door is unlocked now'), nl,
        write('To the east, there is a room where Queen Elsa is being captured.'), nl, !.
        

       

describe(room) :-
        alive(snow_monster),
        at(elsa, in_hand),
        write('The snow_monster sees you and ready to push you down!!!'), nl,
        write('    Ana !!! Kill it to free the Queen Elsa...'), nl,
        die.

describe(snow_monster) :-
        write(' Its a huge snow monster !'), nl.

describe(snow_monster) :-
        alive(snow_monster),
        write('You are in front of a giant snow_monster...'), nl,
        write('Incredible creature.'), nl.



